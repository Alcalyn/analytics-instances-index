<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\InstancesScraper;
use App\Exception\ScrapException;

class InstancesCheckCommand extends Command
{
    protected static $defaultName = 'app:instances:check';

    /**
     * @var InstancesScraper
     */
    private $instancesScraper;

    public function __construct(InstancesScraper $instancesScraper)
    {
        parent::__construct();

        $this->instancesScraper = $instancesScraper;
    }

    protected function configure()
    {
        $this
            ->setDescription('Check an Matomo instance.')
            ->addArgument('url', InputArgument::REQUIRED, 'Url of the Matomo instance to check (i.e https://matomo.example.org/potential-subdomain)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $result = $this->instancesScraper->checkUrl($input->getArgument('url'));
            $io->success('Url seems good.');
            dump(array_keys(get_object_vars($result)), array_values(get_object_vars($result)));
            $io->table(array_keys(get_object_vars($result)), [array_values(get_object_vars($result))]);
        } catch (ScrapException $e) {
            $io->warning($e->getMessage());
        }
    }
}
