<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\InstancesScraper;

class InstancesScrapCommand extends Command
{
    protected static $defaultName = 'app:instances:scrap';

    /**
     * @var InstancesScraper
     */
    private $instancesScraper;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(InstancesScraper $instancesScraper, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->instancesScraper = $instancesScraper;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Scrap all indexed Matomo instances.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $result = $this->instancesScraper->scrapAll(5);

        $this->em->flush();

        $io->success(sprintf('Scraped %d instances (ignored %d because not expired).', $result['updated'], $result['ignored']));
    }
}
