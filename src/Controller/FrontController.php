<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class FrontController
{
    /**
     * @Route("/")
     * @Method("GET")
     * @Template("front/index.html.twig")
     */
    public function index()
    {
        return [];
    }

    /**
     * @Route("/add-my-instance")
     * @Method("GET")
     * @Template("front/add.html.twig")
     */
    public function add()
    {
        return [];
    }
}
