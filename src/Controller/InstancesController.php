<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as FOS;
use App\Entity\Instance;
use App\Service\InstancesScraper;
use App\Repository\InstanceRepository;

/**
 * @Route("/api/instances")
 */
class InstancesController extends FOSRestController
{
    /**
     * @FOS\Get("/", name="instances_list")
     * 
     * @FOS\View()
     */
    public function list(InstanceRepository $instanceRepository)
    {
        return $instanceRepository->findAll();
    }

    /**
     * @FOS\Get("/check", name="instances_check")
     *
     * @FOS\QueryParam(name="url", nullable=false)
     * 
     * @FOS\View()
     */
    public function check(ParamFetcherInterface $fetcher, InstancesScraper $instanceScrapper)
    {
        $canonizedUrl = $instanceScrapper->canonizeUrl($fetcher->get('url'));
        $instance = new Instance($canonizedUrl);
        $instanceScrapper->scrapAndEnrich($instance);

        return $instance;
    }

    /**
     * @FOS\Post("/", name="instances_post")
     *
     * @FOS\RequestParam(name="url", nullable=false)
     * 
     * @FOS\View()
     */
    public function post(ParamFetcherInterface $fetcher, EntityManagerInterface $em, InstancesScraper $instanceScrapper)
    {
        $canonizedUrl = $instanceScrapper->canonizeUrl($fetcher->get('url'));
        $instance = new Instance($canonizedUrl);
        $instanceScrapper->scrapAndEnrich($instance);

        $em->persist($instance);
        $em->flush();
    }
}
