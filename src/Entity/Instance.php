<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstanceRepository")
 */
class Instance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string Url of the Matomo instance
     *
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @JMS\Type("string")
     */
    private $url;

    /**
     * @var string[] Installed plugins on this instance
     *
     * @ORM\Column(type="simple_array", nullable=true)
     *
     * @JMS\Type("array<string>")
     */
    private $plugins = [];

    /**
     * @var bool Is signup enabled
     *
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @JMS\Type("bool")
     */
    private $signupEnabled;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Exclude
     */
    private $lastScrap;

    /**
     * @param string $url
     */
    public function __construct(string $url = null)
    {
        $this->url = $url;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPlugins(): array
    {
        return $this->plugins;
    }

    public function setPlugins(array $plugins): self
    {
        $this->plugins = $plugins;

        return $this;
    }

    public function addPlugin(string $plugin): self
    {
        if (!in_array($plugin, $this->plugins)) {
            $this->plugins []= $plugin;
        }

        return $this;
    }

    public function getSignupEnabled(): ?bool
    {
        return $this->signupEnabled;
    }

    public function setSignupEnabled(?bool $signupEnabled): self
    {
        $this->signupEnabled = $signupEnabled;

        return $this;
    }

    public function getLastScrap(): ?\DateTimeInterface
    {
        return $this->lastScrap;
    }

    public function setLastScrap(?\DateTimeInterface $lastScrap): self
    {
        $this->lastScrap = $lastScrap;

        return $this;
    }
}
