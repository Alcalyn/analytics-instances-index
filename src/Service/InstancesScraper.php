<?php

namespace App\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\ClientInterface;
use App\Exception\ScrapException;
use App\Entity\Instance;
use GuzzleHttp\Exception\RequestException;
use App\Repository\InstanceRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class InstancesScraper
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var InstanceRepository
     */
    private $instanceRepository;

    public function __construct(ClientInterface $client, InstanceRepository $instanceRepository)
    {
        $this->client = $client;
        $this->instanceRepository = $instanceRepository;
    }

    /**
     * Get Matomo Signup plugin response from an url.
     *
     * @param Uri $uri
     *
     * @return \stdClass
     *
     * @throws ScrapException
     */
    public function scrapUri(Uri $uri): \stdClass
    {
        $uri = $uri->withQuery(join('&', [
            'module=API',
            'method=Signup.getSignupPublicSettings',
            'format=json2',
        ]));

        $request = new Request('GET', $uri);

        try {
            $response = $this->client->send($request);
        } catch (RequestException $e) {
            throw new ScrapException('Matomo url seems to be incorrect as host is not responding.', (string) $uri, $e);
        }

        $jsonResponse = json_decode($response->getBody());

        if (!$jsonResponse) {
            throw new ScrapException('Host responds but seems not to be a Matomo instance.', (string) $uri);
        }
        
        if (!isset($jsonResponse->signupPlugin)) {
            throw new ScrapException('Matomo instance responds but Signup plugin seems not to be enabled.', (string) $uri);
        }

        return $jsonResponse;
    }

    /**
     * @param string $url
     *
     * @return \stdClass
     *
     * @throws ScrapException
     */
    public function checkUrl(string $url): \stdClass
    {
        $uri = new Uri($this->canonizeUrl($url));

        return $this->scrapUri($uri);
    }

    /**
     * Scrap a Matomo instance and enrich the entity.
     *
     * @param Instance $instance
     *
     * @throws ScrapException
     */
    public function scrapAndEnrich(Instance $instance)
    {
        $jsonResponse = $this->scrapUri(new Uri($instance->getUrl()));

        $instance->addPlugin('Signup');
        $instance->setSignupEnabled($jsonResponse->allowNewUsersToSignup);

        $instance->setLastScrap(new \DateTime());
    }

    /**
     * @param string $url
     *
     * @return string
     *
     * @throws ScrapException If the $url provided is invalid.
     */
    public function canonizeUrl(string $url): string
    {
        $uri = new Uri($url);

        // Uri put the host in the path if not else part specified
        if ($uri->getHost() === '' && $uri->getPath() !== '') {
            $uri = $uri
                ->withHost($uri->getPath())
                ->withPath('')
            ;

            if ('/' === substr($uri->getHost(), -1)) {
                $uri = $uri->withHost(substr($uri->getHost(), 0, -1));
            }
        }

        $this->scrapUri($uri);

        // Try if https is available
        if ('https' !== $uri->getScheme()) {
            try {
                $this->scrapUri($uri->withScheme('https'));
                $uri = $uri->withScheme('https');
            } catch (ScrapException $e) {
                $uri = $uri->withScheme('http');
            }
        }

        // Try removing trailing slash
        if ('/' === $uri->getPath()) {
            try {
                $this->scrapUri($uri->withPath(''));
                $uri = $uri->withPath('');
            } catch (ScrapException $e) {
            }
        }

        return (string) $uri;
    }

    /**
     * Refresh instances data by scraping instances
     * that have not been scraped for a certain time ($onlyOlderThan).
     *
     * @param integer $onlyOlderThan
     * 
     * @return int[] Array with keys "updated" and "ignored" instances.
     */
    public function scrapAll(int $onlyOlderThan = 86400)
    {
        $instances = $this->instanceRepository->findAll();
        $expires = new \DateTime();
        $expires->sub(new \DateInterval('PT'.$onlyOlderThan.'S'));
        $result = [
            'updated' => 0,
            'ignored' => 0,
        ];

        foreach ($instances as $instance) {
            if ($instance->getLastScrap() > $expires) {
                $result['ignored']++;
                continue;
            }

            $this->scrapAndEnrich($instance);
            $result['updated']++;
        }

        return $result;
    }
}
