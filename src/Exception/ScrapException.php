<?php

namespace App\Exception;

class ScrapException extends \RuntimeException
{
    public function __construct(string $message, string $url, $previous = null)
    {
        parent::__construct("$message URL: $url", 0, $previous);
    }
}
