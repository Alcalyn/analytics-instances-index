/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
import Vue from 'vue';

// any CSS you require will output into a single css file (app.css in this case)
require('../../node_modules/bootstrap/dist/css/bootstrap.css');
require('../../node_modules/open-iconic/font/css/open-iconic-bootstrap.css');
require('../../node_modules/bs-stepper/dist/css/bs-stepper.css');
require('../css/app.css');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
const Stepper = require('bs-stepper');
require('bootstrap');

window.ju = 'ok';

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');


const app = new Vue({
    delimiters: ['${', '}'],
    el: 'main',
    data: {
        instances: [],
    },
});

console.log(app);

fetch('/api/instances')
    .then(response => response.json())
    .then(instances => {
        console.log('fetched instances', instances);
        app.instances = instances;
    })
;

$(document).ready(() => {
    window.stepper = new Stepper($('.bs-stepper')[0], {
        //animation: true
    });
});
