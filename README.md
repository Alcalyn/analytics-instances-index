# Analytics Instances Index

Server to index web analytics self-hosted instances. Let users find an open instance, or share their own.

By analytics instance, I mean a Matomo instance, OpenWebAnalytics instance...

For who this project can be useful:

- You have a website or any internet application you want to analyse users interactions (visits...). You don't want to use google analytics not want to self-host your own instance of analytics server. Go to an index, and use someone else instance that is shared his instance.

- You have installed your own analytics instance, and you're able to let few others people use your instance. You can add your instance url on an index to gain more visibility.

## Install

Requires php, composer, nodejs

```bash
git clone git@gitlab.com:Alcalyn/analytics-instances-index.git
cd analytics-instances-index/

composer install
yarn install

# Run it
bin/console server:start

# Develop on assets
yarn encore dev --watch
```

Then go to <http://127.0.0.1:8000>,

## License

This project is under [AGPL-3.0 license](LICENSE).
